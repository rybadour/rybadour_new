<?php

class HomeController extends BaseController {

	/**
	 * The default about me page. 
	 */
	public function showWelcome()
	{
		return View::make('welcome');
	}

}
