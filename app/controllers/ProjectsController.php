<?php

class ProjectsController extends BaseController {

	public function showProjects()
	{
		return View::make('projects');
	}

	public function showProject($name)
	{
		return View::make('projects/'.$name, array(
			'name' => $name,
		));
	}
}
