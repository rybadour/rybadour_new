@extends('layout')

@section('head')
	{{ HTML::style('css/project.css') }}
@stop

@section('content')
	<div id="header">
		<h2 id="project_name">@yield('title')</h2>
	</div>
@stop
