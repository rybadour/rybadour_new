<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ryan Badour</title>

	{{ HTML::style('css/main.css') }}

	@yield('head')
</head>
<body>
	<h1>Ryan Badour</h1>

	@yield('content')
</body>
</html>
